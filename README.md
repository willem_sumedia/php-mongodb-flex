Google Cloud Platform:

OS: Linux (Debian)

Package Manager: apt

Check for packages: apt-get update

AppEngine: PHP 7.1.0

The PHP 7.1 runtime is available on the App Engine flexible environment, and is currently in beta.

https://cloudplatform.googleblog.com/2017/03/digging-deep-on-php-71-for-google-app.html

Create a new project on Google Cloud AppEngine, e.g. 'php-mongodb-flex'

In Visual Studio Code, inside a folder where you want to keep the project's code, type in the Integrated Terminal:

gcloud components update

followed by:

gcloud init

If the default configuration is not suitable, you will be prompted for a new configuration:

Pick configuration to use:

[1] Re-initialize this configuration [default]
[2] Create a new configuration

Please enter your numeric choice:

For now, type: 2 (followed by ENTER)

Enter configuration name: Names start with a lower case letter and contain only lower case letters a-z, digits 0-9, and hyphens '-':

Type, e.g.: sumedia-config-001

Your current configuration has been set to: [sumedia-config-001]

You can skip diagnostics next time by using the following flag:

gcloud init --skip-diagnostics

Network diagnostic detecs and fixes local network connection issues.
Checking network connection...done.
Reachability Check passed.
Network diagnostic (1/1 checks) passed.

Choose the account you would like to use to perform operations for this configuration:
[1] willem.vanheemstrasystems@gmail.com
[2] Log in with a new account
Please enter your numeric choice: 

Type: 2 (followed by ENTER)

Your browser has been opened to visit:

https://accounts.google.com/o/oauth2/auth?redirect_uri=..........

In the browser, log in with the account chosen for this project (e.g. willem@sumedia.nl).

When prompted, enter your email and password.

Allow Google Cloud SDK to view and manage your applications deployed on Google App Engine, view and manage your Google Compute Engine resources, and view and manage your data across Google Cloud Platform services, by clicking the button 'Allow'.

You will be prompted as follows:

You are logged in as: [willem@sumedia.nl].

Pick cloud project to use:
[1] hello-world-with-nodejs-159123
...
[5] php-mongodb-flex
...
Please enter numeric choice or text value (must exactly match list item):

Type: php-mongodb-flex (followed by ENTER)

You will be prompted as follows:

Your current project has been set to: [php-mongodb-flex].

Not setting default zone/region (this feature makes it easier to use
[gcloud compute] by setting an appropriate default value for the
--zone and --region flag).

See https://cloud.google.com/compute/docs/gcloud-compute section on how to set
default compute region and zone manually. 
If you would like [gcloud init] to be 
able to do this for you the next time you run it, make sure the
Compute Engine API is enabled for your project on the 
https://console.developers.google.com/apis page.



Your Google Cloud SDK is configured and ready to use!

* Commands that require authentication will use willem@sumedia.nl by default

* Commands will reference project `php-mongodb-flex` by default


Run `gcloud help config` to learn how to change individual settings


This gcloud configuration is called [sumedia-config-001]. 

You can create additional configurations if you work with multiple acc
ounts and/or projects.


Run `gcloud topic configurations` to learn more.



Some things to try next:



* Run `gcloud --help` to see the Cloud Platform services you can interact with. 
  And run `gcloud help COMMAND` to get help on any
 gcloud command.

* Run `gcloud topic -h` to learn about advanced features of the SDK like arg files and output formatting

Now inside the project 'php-mongodb-flex' directory create the following files and folders in Visual Studio Code:

webroot/index.php
app.yaml

The content of 'webroot/index.html' can be e.g.:

<?php
  echo "<p>Hello world!</p>";
  echo "<pre>";
  var_dump($_SERVER);

The content of 'app.yaml' should be:

runtime: php
env: flex

runtime_config:
  document_root: webroot

Next, run the following command in Visual Studio Code Intergrated Terminal:

php -v

It should tell you whcih version of PHP it is using, e.g. PHP 5.6.28.

Download and install the latest version of xammp, if that is your local server, from https://www.apachefriends.org/download.html for PHP 7.1.* support

(See also https://github.com/Microsoft/vscode/issues/13356)

If you require PHP 7.1.0, on Windows search for "environment"

- click on edit environment variables (for your acount)
- click on the "path" variable and press "Edit..."
- click on "new"
- write down the path to your php 7 files, for example: D:\php7

(in your case it should be: C:\OpenServer\modules\php\PHP-7-x64 )

When starting Visual Studio Code the error should be gone.

Check by typing again (perhaps in a new terminal window): php -v

NOTE: Make sure you have 'Billing Enabled' for this project (i.e. php-mongodb-flex) before deploying.
You can check this at the 'Billing' sidebar menu option at the Google Cloud Platform Web Console.

Now deploy the app by typing:

gcloud app deploy

You will be prompted as follows:

You are creating an app for project [php-mongodb-flex].
WARNING: Creating an App Engine application for a project is irreversible and the region cannot be changed. 
More information about regions is at https://cloud.google.com/appengine/docs/locations.

Please choose the region where you want your App Engine application located:

[1] us-east1    (supports standard and flexible)
[2] europe-west (supports standard and flexible)
[3] us-central  (supports standard and flexible)
[4] asia-nordeast1 (supports standard and flexible)
[5] cancel
Please enter your numeric choice:

Type for europe-west: 2 (followed by ENTER)

You will be prompted with the following:

Creating App Engine application in project [php-mongodb-flex] and region [europe-west]....done.
You are about to deploy the following services:
 - php-mongodb-flex/default/20170430t150924 (from [C:\Users\user\git\sumedia\php-mongodb-flex\app.yaml])
     Deploying to URL: [https://php-mongodb-flex.appspot.com]

Do you want to continue (Y/n)?

Confirm by typing: Y (followed by ENTER)

You will be prompted by:

If this is your first deployment, this may take a while... 
...
DONE
-----------------------------------------------------------------------------------
Updating service [default]...done.

Deployed service [default] to [https://php-mongodb-flex.appspot.com]

You can stream logs from the command line by running:
  
gcloud app logs tail -s default

To view your application in the web browser run:
  
gcloud app browse

Type: gcloud app browse

You will be prompted with:

Opening [https://php-mongodb-flex.appspot.com] in a new tab in your default browser.

Open the browser (if not done so automatically) and browse for above URL.

You should see the Hello world! message followed by settings of the server.

# Using MongoDB with PHP

See https://cloud.google.com/php/getting-started/deploy-mongodb

If you plan to use an unmanaged MongoDB instance running on Compute Engine, check out Bitnami's preconfigured click-to-deploy solution. We are not choosing 'mLab' managed MongoDB enviroenment as they charge more than Bitnami does.

At Bitnami (https://console.cloud.google.com/launcher/config/bitnami-launchpad/mongodb) create a new MongoDB deployment:

Operating System: Debian (8)

Software: 

- MongoDB (3.4.3)
- OpenSSL (1.0.2k)

Deployment name: mongodb-1

Zone: europe-west1-b

Machine type: micro (0.6 GB memory)

Boot Disk Type: Standard Persistent Disk

Disk size in GB: 10

Networking name: default

Subnetwork name: default

Firewall: Checked Allow TCP 27017 traffic

External IP: Ephemeral

Check to Agree to Terms and Conditions

Click the button 'Implement'.

## Bitname MongoDB Deployment Manager

Browse to https://console.cloud.google.com/deployments/details/mongodb-1

Make a note of the configuration details, such as user accounts and passwords for next steps.

On the Bitnami MongoDB Deployment Manager page, click the button 'SSH':

A terminal window will open and you will be prompted as follows:

Connected, host fingerprint: ssh-rsa 2048 CD:72:FE:5F:26:E9:68:61:C1:17:A0:D3:A5:2C:39:27:98:17:32:5E
The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.
Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
       ___ _ _                   _
      | _ |_) |_ _ _  __ _ _ __ (_)
      | _ \ |  _| ' \/ _` | '  \| |
      |___/_|\__|_|_|\__,_|_|_|_|_|
  *** Welcome to the Bitnami MongoDB 3.4.3-0 ***
  *** Service accessible using hostname 130.211.81.154 port 27017 ***
  *** Documentation:  https://docs.bitnami.com/google/infrastructure/mongodb/ ***
  ***                 https://docs.bitnami.com/google/ ***
  *** Bitnami Forums: https://community.bitnami.com/ ***
willem@mongodb-1-vm:~$

Make a note of the hostname (here: 130.211.81.154) as you need to type that in the file 'config/settings.yml' in the project directory of this project 'php-mongodb-flex' as follows:

...
# configure Mongo backend
mongo_url: mongodb://130.211.81.154:27017
mongo_database: mongodb-1
mongo_collection: books
...

NOTE: port 27017 is the default port for MongoDB.

## Install and enable MongoDB for PHP locally

See http://php.net/manual/en/mongodb.installation.php

Get the MongoDB driver for PHP from http://pecl.php.net/package/mongodb

NOTE: On Windows you can download the DLL directly from above URL. 
Make sure to get the MongoDB driver for PHP 7.1.0, e.g. 7.1 Thread Safe (TS) x86

On Windows, if using XAMMP, place the file 'php_mongodb.dll' into the folder 'C:/xammp/php/ext/'.

Add the following line to your php.ini file:

extension=php_mongodb.dll

Now that the extension is installed, you need to add the MongoDB library to composer as well:

composer require "mongodb/mongodb":"^1.0.0"

Alternatively, add the following line to composer.json:

  "require": {
    ...
    "mongodb/mongodb": "^1.0.0",
    ...
  }

Make sure the file in the root directory of this project 'php-mongodb-flex', called 'php.ini', has at least the following entry:

extension=mongodb.so

## Configuring settings

Go to the php-mongodb-flex directory, and copy the settings.yml.dist file:

cp config/settings.yml.dist config/settings.yml

Open config/settings.yml for editing.

This link explains how the google_client_id and google_client-secret can be set.

https://auth0.com/docs/connections/social/google

For example:

# Google credentials and configuration
google_client_id:      1087266958918-e6ps33q6ermd5a1t1d8gfg3l2eph4arm.apps.googleusercontent.com
google_client_secret:  01oNyIZIVySbP43FfBtuzUjZ
google_project_id:     php-mongodb-flex

Replace YOUR_PROJECT_ID with your project ID.

Set the value of bookshelf_backend to mongodb.

Set the values of mongo_url, mongo_database, and mongo_collection to the appropriate values for your MongoDB instance. For example:

mongo_url: mongodb://130.211.81.154:27017
mongo_database: mongodb-1
mongo_collection: books

Save and close settings.yml.

## Installing dependencies

In the php-mongodb-flex directory, enter this command.

composer install

You will be prompted with a long list of installation results, 
and finally the line 
"Generating autoload files"

All required files will be in the 'vendor' directory (including mongodb) as well as the file 'autoload.php'.

## Running the app on your local machine

Start a local web server:

php -S localhost:8001 -t webroot

In your web browser, enter this address.

http://localhost:8001

Now you can browse the app's web page.

## Deploying the app to the App Engine flexible environment

Deploy the app:

gcloud app deploy

In your web browser, enter this address. Replace [YOUR_PROJECT_ID] with your project ID (i.e. php-mongodb-flex):

https://[YOUR_PROJECT_ID].appspot.com

If you update your app, you can deploy the updated version by entering the same command you used to deploy the app the first time. The new deployment creates a new version of your app and promotes it to the default version. The older versions of your app remain, as do their associated VM instances. Be aware that all of these app versions and VM instances are billable resources.

You can reduce costs by deleting the non-default versions of your app.